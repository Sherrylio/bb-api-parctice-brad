import React, { useState, useEffect } from "react";
import "./App.css";
import Header from "./components/ui/Header";
import Axios from "axios";
import CharacterGrid from "./components/characters/CharacterGrid";
import SearchItems from "./components/ui/SearchItems";

function App() {
  const [items, setItems] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [query, setQuery] = useState("");

  useEffect(() => {
    const fetchItems = async () => {
      const data = await Axios.get(
        `https://www.breakingbadapi.com/api/characters?name=${query}`
      );
      setIsLoading(false);
      setItems(data.data);
    };

    fetchItems();
  }, [query]);
  return (
    <div className="container">
      <Header />
      <SearchItems
        getQuery={(q) => {
          setQuery(q);
        }}
      />
      <CharacterGrid isLoading={isLoading} items={items} />
    </div>
  );
}

export default App;
