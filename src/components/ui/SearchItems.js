import React from "react";

const SearchItems = ({ getQuery }) => {
  const [text, setText] = React.useState();

  const handleChange = (value) => {
    setText(value);
    getQuery(value);
  };
  return (
    <section className="search">
      <form>
        <input
          type="text"
          className="form-control"
          placeholder="Search here"
          value={text}
          autoFocus
          onChange={(e) => {
            handleChange(e.target.value);
          }}
        />
      </form>
    </section>
  );
};

export default SearchItems;
