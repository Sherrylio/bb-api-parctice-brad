import React from "react";
import Characters from "./Characters";
import Spinner from "../ui/Spinner";

const CharacterGrid = ({ items, isLoading }) => {
  return (
    <div>
      {isLoading ? (
        <Spinner />
      ) : (
        <section className="cards">
          {" "}
          {items.map((item) => (
            <Characters key={item.char_id} item={item}></Characters>
          ))}{" "}
        </section>
      )}
    </div>
  );
};

export default CharacterGrid;
